package com.casjp.realmapp.core.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by John Paul Cas on 3/11/2017.
 */

public class Todo extends RealmObject {
    @PrimaryKey
    private long id;
    private String name;

    public Todo() {
    }

    public Todo(Builder builder) {
        setId(builder.id);
        setName(builder.name);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static class Builder {
        private long id;
        private String name;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder todo(String name) {
            this.name = name;
            return this;
        }

        public Todo build() {
            return new Todo(this);
        }

    }
}
