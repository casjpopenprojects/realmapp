package com.casjp.realmapp.core.constant;

/**
 * Created by John Paul Cas on 3/11/2017.
 */

public class RealmDb {

    public static final String DATABASE_NAME = "todos.realm";

    public static final int DATABASE_VERSION = 1;

}
