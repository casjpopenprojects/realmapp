package com.casjp.realmapp.core.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.casjp.realmapp.R;
import com.casjp.realmapp.core.model.Todo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;

/**
 * Created by John Paul Cas on 3/11/2017.
 */

public class TodoAdapter
        extends RecyclerView.Adapter<TodoAdapter.ViewHolder> {

    private Context context;
    private RealmResults<Todo> todos;

    private LayoutInflater inflater;
    private OnClickListener listener;

    public TodoAdapter(Context context, RealmResults<Todo> todos, OnClickListener listener) {
        this.context = context;
        this.todos = todos;

        inflater = LayoutInflater.from(context);

        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = inflater.inflate(R.layout.row_todo_layout, parent, false);

        return new ViewHolder(row);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Todo todo = todos.get(position);
        holder.tvTodoName.setText(todo.getName());
    }

    public void update(RealmResults<Todo> todos) {
        this.todos = todos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return todos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        @BindView(R.id.tvTodoName) TextView tvTodoName;

        public ViewHolder(View row) {
            super(row);
            ButterKnife.bind(this, row);

            row.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(getAdapterPosition());
        }
    }

    public interface OnClickListener {
        void onClick(int itemPosition);
    }

}
