package com.casjp.realmapp.modules.home;

import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.casjp.realmapp.R;
import com.casjp.realmapp.app.RealmApplication;
import com.casjp.realmapp.core.adapters.TodoAdapter;
import com.casjp.realmapp.core.model.Todo;
import com.casjp.realmapp.modules.base.activities.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmResults;

public class MainActivity extends BaseActivity
        implements TodoAdapter.OnClickListener {

    public static final String TAG =
            MainActivity.class.getSimpleName();

    @BindView(R.id.btnAddTodo) Button btnAddTodo;
    @BindView(R.id.rvTodos) RecyclerView rvTodos;
    @BindView(R.id.tietTodoName) TextInputEditText tietTodoName;

    private RealmResults<Todo> todos;

    private TodoAdapter todoAdapter;


    @Override
    protected void initComponents() {
        super.initComponents();
        ButterKnife.bind(this);

        // get all todos
        todos = getAllTodos();

        // setup todoAdapter
        todoAdapter = new TodoAdapter(this, todos, this);

        rvTodos.setLayoutManager(new LinearLayoutManager(this));
        rvTodos.setAdapter(todoAdapter);

        // handle the change TodoObject
        todos.addChangeListener(new RealmChangeListener<RealmResults<Todo>>() {
            @Override
            public void onChange(RealmResults<Todo> element) {
                updateTodos(element);
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        todos.removeAllChangeListeners(); // remove change listener of todo
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @OnClick(R.id.btnAddTodo)
    public void addTodo() {
        if (tietTodoName.getText().toString().isEmpty()) {

            tietTodoName.setError("Field cannot be empty");
        } else {
            saveTodo(tietTodoName.getText().toString());
        }
    }

    @Override
    public void onClick(int itemPosition) {
        Todo todo = todos.get(itemPosition);
        Snackbar.make(findViewById(R.id.activity_main), toStringTodo(todo), Snackbar.LENGTH_SHORT)
                .show();
    }

    /**
     * Update TodoAdapter
     *
     * @param todos
     * new TodoList
     */
    public void updateTodos(RealmResults<Todo> todos) {
        todoAdapter.update(todos);
    }

    /**
     * Get All Todos from database
     *
     * @return
     * All Todos
     */
    private RealmResults<Todo> getAllTodos() {
        realm = Realm.getDefaultInstance(); // get realm instance
        RealmResults<Todo> todos =
                realm.where(Todo.class).findAllAsync();

        realm.close(); // close realm

        return todos;
    }

    /**
     * Save TheTodo to realm database
     *
     * @param todoName The name to be save
     */
    private void saveTodo(String todoName) {
        long todoPrimaryKey = RealmApplication.todoPrimaryKey.getAndIncrement();
        realm.beginTransaction();
        Todo todo = realm.createObject(Todo.class, todoPrimaryKey);
        todo.setName(todoName);

        realm.commitTransaction();
    }

    /**
     * Convert The TodoObject to String format
     *
     * @param todo
     * @return String
     */
    private String toStringTodo(Todo todo) {
        return new StringBuilder("Todo -")
                .append(" Id : ")
                .append(todo.getId())
                .append(" Name : ")
                .append(todo.getName())
                .toString();
    }
}
