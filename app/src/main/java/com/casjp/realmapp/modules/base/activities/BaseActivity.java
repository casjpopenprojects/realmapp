package com.casjp.realmapp.modules.base.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import io.realm.Realm;

/**
 * Created by John Paul Cas on 3/11/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    public Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        if (!realm.isClosed()) realm.close();
        super.onDestroy();
    }

    protected void initComponents() {
        setContentView(getLayoutResource());

        realm = Realm.getDefaultInstance();
    }

    protected abstract int getLayoutResource();
}
