package com.casjp.realmapp.app;

import android.app.Application;

import com.casjp.realmapp.core.constant.RealmDb;
import com.casjp.realmapp.core.model.Todo;

import java.util.concurrent.atomic.AtomicLong;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by John Paul Cas on 3/11/2017.
 */

public class RealmApplication extends Application {

    public static AtomicLong todoPrimaryKey;

    @Override
    public void onCreate() {
        super.onCreate();
        initRealm();
    }

    /**
     * Initialize realm
     */
    private void initRealm() {
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name(RealmDb.DATABASE_NAME)
                .schemaVersion(RealmDb.DATABASE_VERSION)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(config);

        Realm realm = Realm.getDefaultInstance();

        // work around to get and set the primary key
        try {

            todoPrimaryKey = new AtomicLong(realm.where(Todo.class).max("id").longValue() + 1);
        } catch (Exception e) {
            realm.beginTransaction();

            // create a new object when the app started
            Todo todo = new Todo();
            todo.setId(0);
            realm.copyToRealm(todo);
            todoPrimaryKey = new AtomicLong(realm.where(Todo.class).max("id").longValue() + 1);
            // remove the object that added
            RealmResults<Todo> todos =
                    realm.where(Todo.class).equalTo("id", 0).findAll();
            todos.deleteAllFromRealm();

            realm.commitTransaction();
        }



    }
}
